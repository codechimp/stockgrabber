const restify = require('restify');
const { routes } = require('./routes');
const { logger } = require('./logger');
const jobsScheduler = require('./scheduler');

const PORT = 5000;
logger.info(`Starting server on port ${PORT}`);
const app = restify.createServer();
routes(app);
app.listen(PORT);
logger.info(`Server started on port ${PORT}`);

logger.info(`${jobsScheduler.jobs} jobs scheduled`);
