const { googleFinance } = require('./googleFinance');

module.exports = logger => [googleFinance(logger)];
