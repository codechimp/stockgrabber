const google = require('google-finance-data');
const { stocks } = require('../../models');

class GoogleFinance {
  constructor(logger) {
    this.name = 'GoogleFinanceJob';
    this.logger = logger;
  }

  async execute(config) {
    const jobConfig = config || {};
    this.logger.debug(`Config: ${jobConfig}`);
    const stockTickers = await stocks.loadStocks();
    this.logger.info(`Executing Google Finance job for stocks ${stockTickers}`);
    stockTickers.forEach(st => {
      google
        .getSymbol(st.ticker)
        .then(data => {
          this.logger.debug(
            `Retrieved data for ${st.ticker}: ${JSON.stringify(data)}`
          );
          stocks.saveRawData(st, this, data);
        })
        .catch(err =>
          this.logger.error(
            `Recieved error downloading stock ${st.ticker}: ${err}`
          )
        );
    });
  }
}

exports.googleFinance = logger => new GoogleFinance(logger);
