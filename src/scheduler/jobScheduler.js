const schedule = require('node-schedule');
const { jobs } = require('../models');
const { logger } = require('../logger');

const FREQUENCY_PRESETS = {
  MARKET_OPEN_EVERY_5_MIN: ['0', '*/5', '9-17', '*', '*', '1-5']
};

class JobScheduler {
  constructor() {
    this.jobs = jobs;
    this.jobsCache = {};
    this.schedulesCache = {};
  }

  async registerJob(job) {
    if (!job.name) throw new Error('Tried registering a job with no name');
    logger.debug(`Registering job ${job.name}`);

    const jobRec = await jobs.getJob(job.name);
    if (jobRec) this.jobsCache[job.name] = jobRec;
    else {
      if (!job.cronSchedule)
        job.cronSchedule = FREQUENCY_PRESETS.MARKET_OPEN_EVERY_5_MIN;
      this.jobsCache[job.name] = await jobs.saveJob(job);
    }
    logger.debug(
      `Job ${job.name} is being triggered on frequequency ${
        this.jobsCache[job.name].cronSchedule
      }`
    );
    this.schedulesCache[job.name] = schedule.scheduleJob(
      this.jobsCache[job.name].cronSchedule,
      fireDate => {
        logger.debug(`Job ${job.name} was fired on ${fireDate}`);
        job.execute(this.jobsCache[job.name].config);
      }
    );
  }
}

exports.FREQUENCY_PRESETS = FREQUENCY_PRESETS;

exports.jobsScheduler = new JobScheduler();
