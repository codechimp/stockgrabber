const { logger } = require('../logger');
const jobs = require('./jobs')(logger);
const { jobsScheduler } = require('./jobScheduler');

if (jobs) jobs.forEach(job => jobsScheduler.registerJob(job));

module.exports = jobsScheduler;
