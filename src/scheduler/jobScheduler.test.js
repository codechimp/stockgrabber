'use strict';

const { logger } = require('../logger');
const { jobs } = require('../models');
const { jobsScheduler, FREQUENCY_PRESETS } = require('./jobScheduler');

describe('Job Scheduler', () => {
  beforeEach(async () => {
    const allJobs = await jobs.loadJobs();
    allJobs.forEach(async job => await jobs.removeJob(job.name));
  });

  it('should register a new job with no data saved in DB', async () => {
    await jobsScheduler.registerJob({
      name: 'TestJob',
      execute: async config => {}
    });
    const result = await jobs.getJob('TestJob');
    expect(result).toMatchObject({
      name: 'TestJob',
      cronSchedule: FREQUENCY_PRESETS.MARKET_OPEN_EVERY_5_MIN
    });
  });

  it('should register a new job when there is data saved in DB', async () => {
    await jobs.saveJob({
      name: 'TestJob2',
      cronSchedule: ['*', '*', '*', '*', '*', '*'],
      config: { test: 'test' }
    });
    await jobsScheduler.registerJob({
      name: 'TestJob2',
      execute: async config => {}
    });
    const result = await jobs.getJob('TestJob2');
    expect(result).toMatchObject({
      name: 'TestJob2',
      cronSchedule: ['*', '*', '*', '*', '*', '*'],
      config: { test: 'test' }
    });
  });
});
