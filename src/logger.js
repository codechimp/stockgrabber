const winston = require('winston');
const moment = require('moment');

const customLevels = {
  levels: {
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    fatal: 0
  },
  colors: {
    trace: 'white',
    debug: 'gray',
    info: 'green',
    warn: 'yellow',
    error: 'red',
    fatal: 'red'
  }
};

const isProductionEnv = process.env.NODE_ENV === 'production';

const parser = param => {
  if (!param) {
    return '';
  }
  if (typeof param === 'string') {
    return param;
  }
  return Object.keys(param).length ? JSON.stringify(param, undefined, 2) : '';
};

const formatter = winston.format.combine(
  winston.format.colorize(),
  winston.format.timestamp(),
  winston.format.splat(),
  winston.format.printf(info => {
    const { timestamp, level, message, meta } = info;
    const ts = moment(timestamp)
      .local()
      .format('HH:MM:ss');
    const metaMsg = meta ? `: ${parser(meta)}` : '';
    return `${ts} [${level}] ${parser(message)} ${metaMsg}`;
  })
);

class Logger {
  constructor() {
    const transport = new winston.transports.Console({
      format: formatter
    });
    this.logger = winston.createLogger({
      // Logs info, warn, error and fatal for prod and the rest for dev
      level: isProductionEnv ? 'info' : 'trace',
      // We redefine security levels as mentioned
      levels: customLevels.levels,
      transports: [transport]
    });
    winston.addColors(customLevels.colors);
  }

  trace(msg, meta) {
    this.logger.log('trace', msg, meta);
  }

  debug(msg, meta) {
    this.logger.debug(msg, meta);
  }

  info(msg, meta) {
    this.logger.info(msg, meta);
  }

  warn(msg, meta) {
    this.logger.warn(msg, meta);
  }

  error(msg, meta) {
    this.logger.error(msg, meta);
  }

  fatal(msg, meta) {
    this.logger.log('fatal', msg, meta);
  }
}

/**
 * Examples
 */
// logger.trace({ x: 2 }, { a: 1 });
// logger.trace('hi', { a: 1 });
// logger.trace({ x: 2 }, 'by');
// logger.trace('hi again');
// logger.trace('that its', 'by');

exports.logger = new Logger();
