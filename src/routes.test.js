const request = require('supertest');
const restify = require('restify');
const { routes } = require('./routes');

const app = restify.createServer();
routes(app);

describe('API', () => {
  it('should reply with a health check', async () => {
    await request(app)
      .get('/health')
      .expect('Content-Type', /json/)
      .expect(200, {
        status: 'OK'
      });
  });
});
