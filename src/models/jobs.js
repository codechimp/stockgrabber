const { logger } = require('../logger');

const JOBS_TABLE = 'jobs';

const cleanJobDataForDB = obj => {
  return {
    name: obj.name,
    config: obj.config ? JSON.stringify(obj.config) : null,
    cronSchedule: Array.isArray(obj.cronSchedule)
      ? JSON.stringify(obj.cronSchedule)
      : obj.cronSchedule,
    createdAt: obj.createdAt ? obj.createdAt : new Date(),
    updatedAt: obj.updatedAt ? obj.updatedAt : null,
    deletedAt: obj.deletedAt ? obj.deletedAt : null
  };
};

const buildJobFromDB = obj => {
  return {
    name: obj.name,
    config: obj.config ? JSON.parse(obj.config) : null,
    cronSchedule: obj.cronSchedule ? JSON.parse(obj.cronSchedule) : null,
    createdAt: obj.createdAt,
    updatedAt: obj.updatedAt,
    deletedAt: obj.deletedAt
  };
};

class Jobs {
  constructor(knex) {
    this.knex = knex;
  }

  async loadJobs() {
    const results = await this.knex
      .select()
      .table(JOBS_TABLE)
      .then(rows => {
        const jobs = [];
        rows.forEach(r => jobs.push(buildJobFromDB(r)));
        return jobs;
      });
    return results || [];
  }

  async getJob(name) {
    const results = await this.knex
      .select()
      .table(JOBS_TABLE)
      .where({ name });
    return results && results.length === 1 ? buildJobFromDB(results[0]) : null;
  }

  async saveJob(newJob) {
    const data = cleanJobDataForDB(newJob);
    const job = await this.getJob(data.name);
    try {
      if (job) {
        await this.knex.transaction(async trx => {
          await trx(JOBS_TABLE)
            .where({ name: data.name })
            .update(data);
        });
      } else {
        await this.knex.transaction(async trx => {
          await trx(JOBS_TABLE).insert(data);
        });
      }
    } catch (err) {
      logger.error(`Error saving job: ${err}`);
    }
    return buildJobFromDB(data);
  }

  async removeJob(name) {
    await this.knex.transaction(async trx => {
      await trx(JOBS_TABLE)
        .where({ name: name })
        .delete();
    });
  }
}

module.exports = knex => new Jobs(knex);
