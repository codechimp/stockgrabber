const { logger } = require('../logger');

const MONITORED_STOCKS_TABLE_NAME = 'monitored_stocks';
const RAW_STOCK_DATA_TABLE_NAME = 'stocks_raw';

class Stocks {
  constructor(knex) {
    this.knex = knex;
  }

  async loadStocks() {
    const results = await this.knex
      .select()
      .table(MONITORED_STOCKS_TABLE_NAME)
      .then(rows => {
        const stocks = [];
        rows.forEach(r => stocks.push({ ticker: r.ticker, prefix: r.prefix }));
        return stocks;
      });
    return results || [];
  }

  async getStock(stockTicker) {
    const results = await this.knex
      .select()
      .table(MONITORED_STOCKS_TABLE_NAME)
      .where({ ticker: stockTicker });
    return results && results.length === 1 ? results[0] : null;
  }

  async saveStock(stock) {
    const results = await this.getStock(stock.ticker);
    try {
      if (results) {
        await this.knex.transaction(async trx => {
          await trx(MONITORED_STOCKS_TABLE_NAME)
            .where({ ticker: stock.ticker })
            .update(stock);
        });
      } else {
        await this.knex.transaction(async trx => {
          await trx(MONITORED_STOCKS_TABLE_NAME).insert(stock);
        });
      }
    } catch (err) {
      logger.error(`Error saving monitored stock: ${err}`);
    }
    return await this.getStock(stock.ticker);
  }

  async removeStock(stockTicker) {
    await this.knex(MONITORED_STOCKS_TABLE_NAME)
      .where({ ticker: stockTicker })
      .delete();
  }

  async saveRawData(stockTicker, job, data) {
    await this.knex(RAW_STOCK_DATA_TABLE_NAME).insert({
      ticker: stockTicker.ticker,
      job_name: job.name,
      data: JSON.stringify(data),
      createdAt: new Date()
    });
  }

  async retrieveRawData(clause) {
    const whereClause = clause || {};
    const results = await this.knex
      .select()
      .table(RAW_STOCK_DATA_TABLE_NAME)
      .where(whereClause)
      .then(rows => {
        const data = [];
        rows.forEach(r =>
          data.push({
            ticker: r.ticker,
            job_name: r.job_name,
            prefix: r.prefix,
            data: r.data ? JSON.parse(r.data) : null,
            createdAt: r.createdAt
          })
        );
        return data;
      });
    return results || [];
  }

  async clearRawData() {
    await this.knex(RAW_STOCK_DATA_TABLE_NAME).delete();
  }
}

module.exports = knex => new Stocks(knex);
