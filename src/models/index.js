const { knex } = require('./db');
const stock = require('./stocks');
const jobs = require('./jobs');

exports.stocks = stock(knex);
exports.jobs = jobs(knex);
