const config = require('../../../knexfile');
exports.knex = require('knex')(config[process.env.NODE_ENV || 'development']);
