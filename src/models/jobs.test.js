'use strict';

const { knex } = require('./db');
const jobs = require('./jobs')(knex);

describe('Jobs DAO', () => {
  beforeEach(async () => {
    const allJobs = await jobs.loadJobs();
    allJobs.forEach(async job => await jobs.removeJob(job.name));
  });

  it('should allow the creation and removal of a Job', async () => {
    let testJob = await jobs.getJob('TestJobsJob');
    expect(testJob).toBeNull();
    testJob = await jobs.saveJob({
      name: 'TestJobsJob',
      cronSchedule: ['*', '*', '*', '*', '*', '*'],
      config: { test: 'testie' }
    });
    expect(testJob).toMatchObject({
      name: 'TestJobsJob',
      cronSchedule: ['*', '*', '*', '*', '*', '*'],
      config: { test: 'testie' }
    });
    await jobs.removeJob(testJob.name);
    testJob = await jobs.getJob('TestJobsJob');
    expect(testJob).toBeNull();
  });

  it('should allow for a job to be updated', async () => {
    let testJob = await jobs.saveJob({
      name: 'TestJobsJobUpdate',
      cronSchedule: ['*', '*', '*', '*', '*', '*'],
      config: { testing: '123' }
    });
    expect(testJob).toMatchObject({
      name: 'TestJobsJobUpdate',
      cronSchedule: ['*', '*', '*', '*', '*', '*'],
      config: { testing: '123' }
    });
    testJob.cronSchedule = ['1', '2', '3', '4', '5', '6'];
    testJob.config = { testing: 'ABC', moreTesting: '123' };
    testJob = await jobs.saveJob(testJob);
    expect(testJob).toMatchObject({
      name: 'TestJobsJobUpdate',
      cronSchedule: ['1', '2', '3', '4', '5', '6'],
      config: { testing: 'ABC', moreTesting: '123' }
    });
    testJob = await jobs.getJob(testJob.name);
    expect(testJob).toMatchObject({
      name: 'TestJobsJobUpdate',
      cronSchedule: ['1', '2', '3', '4', '5', '6'],
      config: { testing: 'ABC', moreTesting: '123' }
    });
  });
});
