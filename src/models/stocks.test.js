'use strict';

const { knex } = require('./db');
const stocks = require('./stocks')(knex);

describe('Stocks DAO', () => {
  beforeEach(async () => {
    await stocks.clearRawData();
  });

  it('should be seeded with a few stocks', async () => {
    const allStocks = await stocks.loadStocks();
    expect(allStocks.length).toBeGreaterThan(0);
    const apple = await stocks.getStock('AAPL');
    expect(apple).toMatchObject({ ticker: 'AAPL' });
  });

  it('should allow for monitored stocks to be added and removed', async () => {
    let fakeStock = await stocks.getStock('FAKE');
    expect(fakeStock).toBeNull();
    fakeStock = await stocks.saveStock({ ticker: 'FAKE' });
    expect(fakeStock).toMatchObject({ ticker: 'FAKE' });
    fakeStock = await stocks.getStock('FAKE');
    expect(fakeStock).toMatchObject({ ticker: 'FAKE' });
    await stocks.removeStock(fakeStock.ticker);
    fakeStock = await stocks.getStock('FAKE');
    expect(fakeStock).toBeNull();
  });

  it('should allow for a monitored stock to be updated', async () => {
    await stocks.saveStock({ ticker: 'UPDT' });
    expect(await stocks.getStock('UPDT')).toMatchObject({ ticker: 'UPDT' });
    await stocks.saveStock({ ticker: 'UPDT', prefix: 'PRE' });
    expect(await stocks.getStock('UPDT')).toMatchObject({
      ticker: 'UPDT',
      prefix: 'PRE'
    });
  });

  it('should allow for raw stock data to be saved and retrieved', async () => {
    expect(await stocks.retrieveRawData()).toMatchObject([]);
    await stocks.saveRawData(
      { ticker: 'AAPL' },
      { name: 'TestJob' },
      { blah: 'blat' }
    );
    const results = await stocks.retrieveRawData({ ticker: 'AAPL' });
    expect(results[0]).toMatchObject({
      ticker: 'AAPL',
      job_name: 'TestJob',
      data: { blah: 'blat' }
    });
  });
});
