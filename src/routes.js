const { logger } = require('./logger');
const { jobs, stocks } = require('./models');

exports.routes = app => {
  app.get('/health', async (req, res, next) => {
    try {
      const allStocks = await stocks.loadStocks();
      const allJobs = await jobs.loadJobs();
      res.send(200, { status: 'OK' });
      logger.trace('Completed health check successfully');
    } catch (err) {
      res.send(500, { status: 'FAILED' });
      logger.error(`Health check failed: ${err}`);
    }
    return next();
  });
};
