// Update with your config settings.

module.exports = {
  test: {
    client: 'sqlite3',
    connection: {
      filename: './mydb.sqlite',
    },
    useNullAsDefault: true,
  },

  development: {
    client: 'mysql',
    connection: {
      host: 'localhost',
      user: 'stockgrabber',
      password: 'stockgrabber',
      database: 'stockgrabber',
    },
  },

  production: {
    client: 'mysql',
    connection: {
      host: 'stockgrabber-mysql.default',
      user: 'stockgrabber',
      password: 'stockgrabber',
      database: 'stockgrabber',
    },
    pool: {
      min: 2,
      max: 10,
    },
  },
};
