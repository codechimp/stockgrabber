exports.seed = knex => {
  return knex('monitored_stocks')
    .select()
    .then(rows => {
      if (rows.length == 0) {
        return knex('monitored_stocks').insert([
          { ticker: 'AAPL' },
          { ticker: 'MSFT' },
          { ticker: 'GOOGL' },
          { ticker: 'DIS' },
          { ticker: 'SBUX' },
          { ticker: 'BAC' },
          { ticker: 'AMZN' },
          { ticker: 'NFLX' },
          { ticker: 'FB' },
          { ticker: 'BABA' }
        ]);
      } else {
        console.log('No stocks seeded');
        return null;
      }
    });
};
