exports.up = knex => {
  return knex.schema
    .createTable('jobs', t => {
      t.string('name')
        .notNullable()
        .primary();
      t.json('config').nullable();
      t.json('cronSchedule').notNullable();
      t.dateTime('createdAt')
        .notNullable()
        .defaultTo(knex.fn.now());
      t.dateTime('updatedAt').nullable();
      t.dateTime('deletedAt').nullable();
    })
    .createTable('monitored_stocks', t => {
      t.string('ticker')
        .notNullable()
        .primary();
      t.string('prefix').nullable();
    })
    .createTable('stocks_raw', t => {
      t.increments('id').primary();
      t.string('ticker').notNullable();
      t.string('job_name').notNullable();
      t.json('data').notNullable();
      t.dateTime('createdAt')
        .notNullable()
        .defaultTo(knex.fn.now());

      t.foreign('ticker')
        .references('ticker')
        .inTable('monitored_stocks');
      t.foreign('job_name')
        .references('name')
        .inTable('jobs');
    });
};

exports.down = knex => {
  console.log('Running knex down');
  return knex.schema
    .dropTableIfExists('stocks_raw')
    .dropTableIfExists('jobs')
    .dropTableIfExists('monitored_stocks');
};
